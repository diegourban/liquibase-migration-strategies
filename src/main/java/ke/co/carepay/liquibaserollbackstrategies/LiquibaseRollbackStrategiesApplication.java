package ke.co.carepay.liquibaserollbackstrategies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiquibaseRollbackStrategiesApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiquibaseRollbackStrategiesApplication.class, args);
	}

}
