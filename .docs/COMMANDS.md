# Liquibase Commands

## Using Gradle

- `.\gradlew.bat status` - Outputs count of unrun change sets.
- `.\gradlew.bat update` - Updates the database to the current version.
- `.\gradlew.bat updateSQL` - Writes SQL to update the database to the current version to STDOUT.
- `.\gradlew.bat rollbackCount -PliquibaseCommandValue=N` -Rolls back the last
  \<liquibaseCommandValue\> change sets.
- `.\gradlew.bat rollbackCountSQL -PliquibaseCommandValue=N` - Writes SQL to roll back the last
  \<liquibaseCommandValue\> change sets to STDOUT.
- `.\gradlew.bat futureRollbackCountSQL -PliquibaseCommandValue=N` - Writes SQL to roll back
  \<liquibaseCommandValue\> changes the database after the changes in the changelog have been
  applied.
- `.\gradlew.bat updateTestingRollback` - Updates the database, then rolls back changes before
  updating again.

More information at:
https://github.com/liquibase/liquibase-gradle-plugin/blob/master/src/main/groovy/org/liquibase/gradle/LiquibasePlugin.groovy

## CLI superpowers

Installation: https://docs.liquibase.com/concepts/installation/home.html

### Using arguments

```
liquibase \
--classpath=path/to/changelog/files \
update \
--changelog-file=com/example/db.changelog.xml \
--url="jdbc:oracle:thin:@localhost:1521:oracle" \
--username=scott \
--password=tiger
```

#### status example

`liquibase --classpath=src/main/resources/db/changelog status --changelog-file=src/main/resources/db/changelog/db.changelog-master.xml --url="jdbc:mariadb://localhost:3306/dev_sample" --username=app --password=secret!123`

### Using liquibase.properties

#### status example

`liquibase status --defaultsFile=C:\Users\DiegoUrban\.liquibase\liquibase-dev.properties`

#### rollbackSQL example

`liquibase rollbackCountSQL 5 --outputFile rollback-output.sql --defaultsFile=C:\Users\DiegoUrban\.liquibase\liquibase-dev.properties`

#### rollback example

`liquibase rollbackCount 1 --defaultsFile=C:\Users\DiegoUrban\.liquibase\liquibase-dev.properties`

More on CLI commands: https://docs.liquibase.com/commands/community/home.html